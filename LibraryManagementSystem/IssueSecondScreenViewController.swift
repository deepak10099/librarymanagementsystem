//
//  ViewController.swift
//  LibraryManagementSystem
//
//  Created by Deepak on 26/09/16.
//  Copyright © 2016 Deepak. All rights reserved.
//

import UIKit
import CoreData

class IssueSecondScreenViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate {

    @IBOutlet weak var bookIdToBeIssuedTextField: UITextField!
    var userIdInWhichBookIsToBeIssued:String?
    var fetchedResultsController:NSFetchedResultsController<NSFetchRequestResult>?{
        get{
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedObjectContext = appDelegate.persistentContainer.viewContext
            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Books")
            let entity = NSEntityDescription.entity(forEntityName: "Books", in: managedObjectContext)
            fetchRequest.entity = entity

            let sortDescriptor = NSSortDescriptor(key: "bookName", ascending: false)
            fetchRequest.sortDescriptors = NSArray(object: sortDescriptor) as? [NSSortDescriptor]
            fetchRequest.fetchBatchSize = 20

            let predicate:NSPredicate = NSPredicate(format: "user.userId = %@",userIdInWhichBookIsToBeIssued!)
            fetchRequest.predicate = predicate
            let fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            self.fetchedResultsController = fetchResultsController
            return self.fetchedResultsController
        }
        set{

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController?.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        do{
            try fetchedResultsController?.performFetch()
        }
        catch
        {

        }
    }
    @IBAction func issueButtonTapped(_ sender: AnyObject) {
        fetchedResultsController?.object(at: <#T##IndexPath#>)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController?.sections?[section]
        return (sectionInfo?.numberOfObjects)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "booksWithSelectedUser")
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "booksWithSelectedUser")
        }
        let fetchedBook:Books = self.fetchedResultsController?.object(at: indexPath) as! Books
        cell?.textLabel?.text = fetchedBook.bookId
        cell?.detailTextLabel?.text = fetchedBook.bookName
        return cell!
    }
}

