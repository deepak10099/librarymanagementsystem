//
//  ViewController.swift
//  LibraryManagementSystem
//
//  Created by Deepak on 26/09/16.
//  Copyright © 2016 Deepak. All rights reserved.
//

import UIKit
import CoreData

class AddBookViewController: UIViewController {

    @IBOutlet weak var bookIdTextField: UITextField!
    @IBOutlet weak var bookNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func addBookButtonTapped(_ sender: AnyObject) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let bookToBeAddedToCoreData:Books = NSEntityDescription.insertNewObject(forEntityName: "Books", into: managedObjectContext) as! Books
        bookToBeAddedToCoreData.bookId = bookIdTextField.text!
        bookToBeAddedToCoreData.bookName = bookNameTextField.text
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

