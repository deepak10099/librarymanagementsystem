//
//  ViewController.swift
//  LibraryManagementSystem
//
//  Created by Deepak on 26/09/16.
//  Copyright © 2016 Deepak. All rights reserved.
//

import UIKit

class IssueFirstScreenViewController: UIViewController {

    @IBOutlet weak var userIdInWhichBookIsToBeIssued: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func issueToUserAccountButtonTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "loginToUserAccount", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationViewController: IssueSecondScreenViewController = segue.destination as! IssueSecondScreenViewController
        destinationViewController.userIdInWhichBookIsToBeIssued = self.userIdInWhichBookIsToBeIssued.text
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

