//
//  Books+CoreDataClass.swift
//  LibraryManagementSystem
//
//  Created by Deepak on 26/09/16.
//  Copyright © 2016 Deepak. All rights reserved.
//

import Foundation
import CoreData

public class Books: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Books> {
        return NSFetchRequest<Books>(entityName: "Books");
    }

    @NSManaged public var bookId: String?
    @NSManaged public var bookName: String?
    @NSManaged public var user: Users?
}
